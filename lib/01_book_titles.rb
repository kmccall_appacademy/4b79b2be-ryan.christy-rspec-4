class Book
  attr_reader :title

  def title=(string)
    cnp = ["a", "an", "and", "in", "of", "the"]
    array = string.split(' ')
    array.each {|w| w.capitalize! unless cnp.include?(w.downcase)}
    array[0].capitalize!
    @title = array.join(' ')
  end
  
end
