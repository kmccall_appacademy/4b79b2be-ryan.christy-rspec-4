class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(Hash)
      @entries.merge!(entry)
    elsif entry.is_a?(String)
      @entries.merge!({entry => nil})
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.include?(key)
  end

  def find(string)
    fidx = []
    kvpairs = []
    array = @entries.keys
    array.each do |e|
      fidx.push(array.index(e)) if e.include?(string)
    end
    fidx.each { |idx| kvpairs.push(@entries.to_a[idx]) }
    kvpairs.to_h
  end

  def printable
    printables = ""
    @entries.sort.each { |k, v| printables += "[#{k}] \"#{v}\"\n" }
    printables.chomp
  end
end
