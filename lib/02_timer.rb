class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 60 / 60
    minutes = (@seconds / 60) - (hours * 60)
    seconds = @seconds % 60
    hrs = hours.to_s.rjust(2, "0")
    min = minutes.to_s.rjust(2, "0")
    sec = seconds.to_s.rjust(2, "0")
    "#{hrs}:#{min}:#{sec}"
  end
end
