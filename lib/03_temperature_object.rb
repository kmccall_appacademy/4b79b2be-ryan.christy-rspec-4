class Temperature
  # TODO: your code goes here!
  attr_accessor :f, :c

  def initialize(options={})
    options = {f: nil, c: nil}.merge(options)
    @f = options[:f]
    @c = options[:c]
  end

  def in_fahrenheit
    if @c
      @c * (9.0/5.0) + 32
    else
      @f
    end
  end

  def in_celsius
    if @f
      (@f - 32.0) * (5.0/9.0)
    else
      @c
    end
  end

  def self.from_celsius(tc)
    Temperature.new({c: tc})
  end

  def self.from_fahrenheit(tf)
    Temperature.new({f: tf})
  end

  def self.ftoc(tf)
    @f = tf
    (@f - 32.0) * (5.0/9.0)
  end

  def self.ctof(tc)
    @c = tc
    @c * (9.0/5.0) + 32
  end
end

class Celsius < Temperature
  attr_accessor :c
  def initialize(optc)
    options = {c: optc}
    @c = options[:c]
  end
end

class Fahrenheit < Temperature
  attr_accessor :f
  def initialize(optf)
    options = {f: optf}
    @f = options[:f]
  end
end
